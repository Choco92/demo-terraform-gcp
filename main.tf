terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
  backend "gcs" {
    bucket = "backend-gcp-ulrich-test" # doit être crée à la main via la console
    prefix = "terraform/state"
  }
}

provider "google" {
  project = "terraform-379914"
  region  = "us-central1"
  zone    = "us-central1-c"
  # credentials = "${file("${var.GCLOUD_KEYFILE_JSON}")}"
}

resource "google_compute_instance" "default" {
  count        = 0
  name         = "terraform-instance"
  machine_type = "e2-micro"
  tags         = ["gcp-instance"]
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }
  network_interface {
    # A default network is created for all GCP projects
    network = google_compute_network.vpc_network.self_link
    # network = "default"
    access_config {
    }
  }
}

resource "google_compute_network" "vpc_network" {
  name                    = "terraform"
  auto_create_subnetworks = "true"
}
